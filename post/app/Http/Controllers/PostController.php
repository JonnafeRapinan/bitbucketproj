<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Post;

class PostController extends Controller
{
    public function getPost(){
    	$posts = DB::table('posts')
    			->join('users', 'users.id', '=', 'posts.userid')
    			->select('posts.id as postid', 'posts.*', 'users.id as userid', 'users.*')
    			->orderBy('posts.created_at', 'desc')
    			->get();

		return view('postlist', compact('posts'));
    }

    public function post(Request $request){
    	if ($request->ajax()){
    		$user = Auth::user();
	    	$post = new Post;

	    	$post->userid = $user->id;
	    	$post->post = $request->input('post');

	  		$post->save();
    		
    		return response($post);
    	}
    }
}
