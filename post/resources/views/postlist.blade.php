@foreach($posts as $post)
	<div class="panel panel-default">
		<div class="panel-body">
			<p style="font-size:16px;"><b>{{ $post->name }}</b> added a post.</p>
			<p style="font-size:11px; margin-top:-10px;">{{ date('M d, Y h:i A', strtotime($post->created_at)) }}</p>
			<h3 style="padding-top:30px; padding-bottom:30px;">{{ $post->post }}</h3>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-2">
					<button class="btn btn-primary btn-sm"><i class="fa fa-thumbs-up"></i> <span>Like</span></button>
				</div>
				<div class="col-md-10" style="margin-left:-40px;">
					<button type="button" class="btn btn-primary btn-sm comment" value="{{ $post->postid }}"><i class="fa fa-comments"></i> Comment</button>
				</div>
			</div>
		</div>
	</div>
	<div class="" id="comment_{{ $post->postid }}">
	</div>
@endforeach